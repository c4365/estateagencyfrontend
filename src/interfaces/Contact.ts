export interface Contact {
  contact_name: string;
  contact_surname: string;
  contact_email: string;
  contact_phone: string;
}
