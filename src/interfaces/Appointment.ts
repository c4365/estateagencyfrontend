export interface Appointment {
  appointment_postcode: string;
  appointment_date: string;
  agent_id: any;
  contact_id: any;
}
