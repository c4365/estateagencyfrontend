import 'bootstrap'
import './scss/style.scss'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

// Mapbox Config
import VueMapboxTs from 'vue-mapbox-ts'

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(VueMapboxTs)

app.mount('#app')
