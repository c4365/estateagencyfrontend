import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: () => import('../views/DashboardView.vue')
    },
    {
      path: '/appointment',
      children: [
        {
          path: 'create',
          name: 'appointment_create',
          component: () => import('../views/CreateAppointment.vue'),
        },
        {
          path: 'list',
          name: 'appointment_list',
          component: () => import('../views/Appointments.vue'),
        },
      ]
    }
  ]
})

export default router
