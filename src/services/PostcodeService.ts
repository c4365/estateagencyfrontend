import axios from 'axios'

const getDetailsByPostcode = async (postCode: string) => {
  if(postCode.trim() === '' || postCode.trim() === null) {
    return
  }
  const url = 'https://api.postcodes.io/postcodes/' + postCode.replace(/\s/g,'');
  const response = await axios.get(encodeURI(url))

  return response.data.result;
}

const getPostcodeByLatLng = async (lat: number, lng: number) => {
  const url = 'https://api.postcodes.io/postcodes/?lon=' + lng + '&lat=' + lat;
  const response = await axios.get(url)
  return response.data.result;
}


export const PostcodeService = {
  getDetailsByPostcode,
  getPostcodeByLatLng
}
