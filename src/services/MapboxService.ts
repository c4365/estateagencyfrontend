import axios from 'axios'

const accessToken: string = 'pk.eyJ1IjoiZW5lc3l1cnRsdSIsImEiOiJja3F1NGV4dDMwMTBsMnVsaHc4Y2E1dDJpIn0.IiSFUA5V4MFMEDIlJ2g43g'
const baseUrl: string = 'https://api.mapbox.com/directions/v5/mapbox/'
const baseLocation = {
  postCode: 'cm27pj',
  latitude: 51.729157,
  longitude: 0.478027
}

const getDistance = async (waypoints: any, profile = 'driving') => {
  let waypointsUrlParam = baseLocation.longitude + ',' + baseLocation.latitude;
  waypoints.forEach(waypoint => {
    waypointsUrlParam += ';' + waypoint.longitude + ',' + waypoint.latitude
  });

  const url = baseUrl + profile + '/' + waypointsUrlParam + '?alternatives=true&continue_straight=true&geometries=geojson&language=en&overview=full&steps=false&access_token=' + accessToken;

  // url += '&depart_at=2024-01-13T20:26';

  const response = await axios.get(encodeURI(url))

  return response.data
}

export const MapboxService = {
  accessToken,
  getDistance,
  baseLocation
}
