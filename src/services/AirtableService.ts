import Airtable from 'airtable'
import type { Appointment } from '@/interfaces/Appointment'
import type { Contact } from '@/interfaces/Contact'
import type { Agent } from '@/interfaces/Agent'

const airtableToken = 'pataeyGX228V3hIL9.283cdf1db4bb6e4390915553e5820ac156fa217c1856e24959ef73fe8dec61d6'
const base = new Airtable({
  apiKey: airtableToken,
  endpointUrl: 'https://api.airtable.com'
}).base(
  'appgykZBGTF92MnHu'
)

const getRecords = (table = '') => {
  return base(table).select({
    offset: 0,
    maxRecords: 1000,
    view: 'Grid view'
  })

}

const createAppointment = (appointment: Appointment) => {
  base('Appointments').create(
    { ...appointment },
    function(err, record) {
      if (err) {
        console.error(err)
        return
      }
      console.log(record)
    }
  )
}

const createContact = (contact: Contact) => {
  base('Contacts').create(
    { ...contact },
    function(err, record) {
      if (err) {
        console.error(err)
        return
      }
    }
  )
}

const createAgent = (agent: Agent) => {
  base('Agents').create(
    { ...agent },
    function(err, record) {
      if (err) {
        console.error(err)
        return
      }
      console.log(record)
    }
  )
}

export const AirtableService = {
  getRecords,
  createAppointment,
  createAgent,
  createContact
}
